import asyncio

def D1(func):
    async def wrapper(*args, **kwargs):
        print("Before calling the decorated method")
        result = await func(*args, **kwargs)
        print("After calling the decorated method")
        return result
    return wrapper

async def M1():
    await asyncio.sleep(1)
    print("M1 executed")

@D1
async def M2():
    print("M2 executing")
    await M1()
    print("M2 executed")

asyncio.run(M2())
