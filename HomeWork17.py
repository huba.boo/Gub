# Версія з багами https://qauto2.forstudy.space/ ( login:guest, password:welcome2qauto)
# Логи https://qauto2.forstudy.space/api-logs
# API документація https://qauto2.forstudy.space/api-docs/
# Написати тести на створювання, редагування, видалення автівки
#
# Для тестів на редагування та видалення використовувати АПІ для створення автівки

import requests
import unittest

base_url = "https://qauto2.forstudy.space/api"

username = "guest"
password = "welcome2qauto"

class CarAPITestCase(unittest.TestCase):
    def setUp(self):
        self.session = requests.Session()
        self.login()

    def tearDown(self):
        self.logout()

    def login(self):
        login_url = f"{base_url}/auth/login"
        data = {
            "username": username,
            "password": password
        }
        response = self.session.post(login_url, json=data)
        self.assertEqual(response.status_code, 200)

    def logout(self):
        logout_url = f"{base_url}/auth/logout"
        response = self.session.post(logout_url)
        self.assertEqual(response.status_code, 200)

    def test_create_car(self):
        create_car_url = f"{base_url}/car"
        data = {
            "make": "Toyota",
            "model": "Camry",
            "year": 2022
        }
        response = self.session.post(create_car_url, json=data)
        self.assertEqual(response.status_code, 201)
        self.assertIn("id", response.json())

    def test_edit_car(self):
        car_id = self.create_test_car()
        edit_car_url = f"{base_url}/car/{car_id}"
        data = {
            "make": "Honda",
            "model": "Accord",
            "year": 2023
        }
        response = self.session.put(edit_car_url, json=data)
        self.assertEqual(response.status_code, 200)
        updated_car = self.get_car_details(car_id)
        self.assertEqual(updated_car["make"], "Honda")
        self.assertEqual(updated_car["model"], "Accord")
        self.assertEqual(updated_car["year"], 2023)

    def test_delete_car(self):
        car_id = self.create_test_car()
        delete_car_url = f"{base_url}/car/{car_id}"
        response = self.session.delete(delete_car_url)
        self.assertEqual(response.status_code, 200)
        self.assertIsNone(self.get_car_details(car_id))

    def create_test_car(self
