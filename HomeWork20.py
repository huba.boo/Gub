# 1.Create connection to database using ponyORM
# 2.Create models classes
# 3.Get data from database different queries:
#     a.Table.select(lambda entity: entity)
#     b.select(entity for entity in Table)

pip install pony

from pony.orm import *

db = Database("<database>://<username>:<password>@<host>/<database_name>")

class Table(db.Entity):
    column1 = Required(str)
    column2 = Required(int)
    # Add more columns as needed

db.generate_mapping(create_tables=True)

with db_session:
    result = select(lambda entity: entity for entity in Table)
    for row in result:
        print(row.column1, row.column2)

with db_session:
    result = select(entity for entity in Table)
    for row in result:
        print(row.column1, row.column2)
