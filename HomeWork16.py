# Написати тести на створювання користувача, та логын у систему.
# Після цього видалити усі ТЕСТОВІ данні з БДхи
# https://qauto2.forstudy.space/api-docs/ ( login:guest, password:welcome2qauto)

import requests
import json
import unittest

class UserTest(unittest.TestCase):
    BASE_URL = "https://qauto2.forstudy.space/api"
    LOGIN_ENDPOINT = "/login"
    USERS_ENDPOINT = "/users"

    def setUp(self):
        self.session = requests.Session()
        self.login()

    def tearDown(self):
        self.logout()

    def login(self):
        url = self.BASE_URL + self.LOGIN_ENDPOINT
        data = {"login": "guest", "password": "welcome2qauto"}
        response = self.session.post(url, data=data)
        self.assertEqual(response.status_code, 200)

    def logout(self):
        url = self.BASE_URL + self.LOGIN_ENDPOINT
        response = self.session.delete(url)
        self.assertEqual(response.status_code, 204)

    def test_create_user(self):
        url = self.BASE_URL + self.USERS_ENDPOINT
        data = {"name": "Test User", "email": "test.user@example.com", "password": "password123"}
        response = self.session.post(url, json=data)
        self.assertEqual(response.status_code, 201)

    def test_login_user(self):
        url = self.BASE_URL + self.LOGIN_ENDPOINT
        data = {"login": "test.user@example.com", "password": "password123"}
        response = self.session.post(url, data=data)
        self.assertEqual(response.status_code, 200)

if __name__ == '__main__':
    unittest.main()

def delete_all_users(self):
    url = self.BASE_URL + self.USERS_ENDPOINT
    response = self.session.get(url)
    data = json.loads(response.text)
    for user in data:
        response = self.session.delete(url + '/' + str(user['id']))
        self.assertEqual(response.status_code, 204)
