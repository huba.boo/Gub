# Create test report with pytest-html
# Install Allure
# Create allure report
# Create steps for the AuthenticationTest class, using facades

pip install pytest-html

import pytest

@pytest.fixture
def login():
    yield

class TestAuthentication:
    def test_login(self, login):
        assert True

    def test_logout(self, login):
        assert True

pytest --html=report.html

pip install allure-pytest

import pytest
import allure

@pytest.fixture
def login():
    yield

@allure.feature('Authentication')
class TestAuthentication:
    @allure.story('Successful login')
    def test_login(self, login):
        allure.step('Step 1: Enter username')
        allure.step('Step 2: Enter password')
        allure.step('Step 3: Click login button')
        assert True

    @allure.story('Successful logout')
    def test_logout(self, login):
        allure.step('Step 1: Click logout button')
        assert True

pytest --alluredir=report

allure serve report
