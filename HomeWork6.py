# 1. створити АРІ на базі гугл таблиці, містить поля "назва товару", "опис товару", "ціна"
# (інтова чи флоат), "залишок" (інтовий чи флоат), "містить глютен" (булеве тру чи фолс (виставляєте прапорцем).
# заповнити мінімум 10 позицій. дані мають бути отримати по зовнішньому ключу "goods" (not "data")
# 2. за допомогою requests завантажити створені дані. порахувати вартість всіх товарів та товарів без глютена.

import requests

url = "https://script.googleusercontent.com/macros/echo?user_content_key=SAuyROyT3Z1Z8RoTxsONYGqcF_2kdLCRAy39E8KbAZvEBMlo7TKWufJD-haf56NPEy8W6i2S_z5IL3UXYtfX2_78uKgVvlc-m5_BxDlH2jW0nuo2oDemN9CCS2h10ox_1xSncGQajx_ryfhECjZEnKj4jwEAkt0qTDNfwAyE8NRHcb9nc5vjGrOftvfgw7ch-q_ZT3SUZvcoF5ogcIsQb34yN3e3p5bPDeMLKCsGtJvx2EBEVJcrftz9Jw9Md8uu&lib=MBn39LUXh3KEcVoiQkaIh-GXr8NvE13i2"
response = requests.get(url)
goods = response.json()

goods = []

total_cost = goods['Price'].sum()
gluten_free_cost = goods['Gluten-free'] == 'yes', 'Price'.sum()
print(f'Total cost: {total_cost}')
print(f'Gluten-free cost: {gluten_free_cost}')

# ok