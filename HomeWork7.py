# 1. Напишіть функцію, яка приймає два аргументи.
# - Якщо обидва аргумени відносяться до числових типів функція пермножує ці аргументи і повертає результат
# - Якшо обидва аргументи відносяться до типу стрінг функція обʼєднує їх в один і повертає
# - В будь-якому іншому випадку - функція повертає кортеж з двох агрументів

def multiply_or_concat(arg1, arg2):
    if isinstance(arg1, (int, float)) and isinstance(arg2, (int, float)):
        return arg1 * arg2
    elif isinstance(arg1, str) and isinstance(arg2b, str):
        return arg1 + arg2
    else:
        return (arg1, arg2)


# 2. Візьміть код з заняття і доопрацюйте натупним чином:
# - користувач має вгадати чизло за певну кількість спроб. користувач на початку програми визначає кількість спроб
# - додайте підказки. якщо рвзнися між числами більше 10 - "холодно", від 10 до 5 - "тепло", 1-4 - "гаряче"

import random

def get_hint(guess, answer):
    diff = abs(guess - answer)
    if diff > 10:
        return "Холодно"
    elif diff >= 5:
        return "Тепло"
    elif diff >= 1:
        return "Гаряче"
    else:
        return "Вітаю! Ви вгадали число!"
max_guesses = int(input("Скільки спроб ви хочете мати? "))
answer = random.randint(1, 100)
for attempt in range(max_guesses):
    print(f"Спроба {attempt + 1} з {max_guesses}")
    guess = int(input("Вгадайте число від 1 до 100: "))
    if guess == answer:
        print(get_hint(guess, answer))
        break
    else:
        print(get_hint(guess, answer))
if guess != answer:
    print(f"Ви не вгадали число {answer}")
