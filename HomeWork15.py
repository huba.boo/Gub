# Create class Figure, and its heirs - Triangle(Figure), Circle(Figure), Rectangle(Figure), and Square(Rectangle)
# Add methods to get square and perimeter for each of them

class Figure:
    def get_square(self):
        pass

    def get_perimeter(self):
        pass


class Triangle(Figure):
    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c

    def get_square(self):
        p = self.get_perimeter() / 2
        return (p * (p - self.a) * (p - self.b) * (p - self.c)) ** 0.5

    def get_perimeter(self):
        return self.a + self.b + self.c


class Circle(Figure):
    def __init__(self, radius):
        self.radius = radius

    def get_square(self):
        return 3.14 * (self.radius ** 2)

    def get_perimeter(self):
        return 2 * 3.14 * self.radius


class Rectangle(Figure):
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def get_square(self):
        return self.a * self.b

    def get_perimeter(self):
        return 2 * (self.a + self.b)


class Square(Rectangle):
    def __init__(self, a):
        super().__init__(a, a)
