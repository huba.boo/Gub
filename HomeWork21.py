# 1. Install mongo
# 2. Create python project, install pymongo (pip install pymongo)
# 3. Create json file with data (one of objects should contain at least 3 levels of nesting (dict in dict in array in dict as example))
# 4. Insert data to db using python and json
# 5. Create Model for object from step 3
# 6. Get this object from DB and convert it to object


myenv\Scripts\activate

pip install pymongo

{
  "name": "John Smith",
  "age": 30,
  "address": {
    "street": "123 Main St",
    "city": "Exampleville",
    "country": "Exampleland",
    "contacts": [
      {
        "type": "email",
        "value": "john@example.com"
      },
      {
        "type": "phone",
        "value": "555-1234"
      }
    ]
  }
}

import json
from pymongo import MongoClient

client = MongoClient()
db = client.mydatabase

with open('data.json') as f:
    data = json.load(f)

collection = db.mycollection
collection.insert_one(data)

client.close()

class Person:
    def __init__(self, name, age, address):
        self.name = name
        self.age = age
        self.address = address


class Address:
    def __init__(self, street, city, country, contacts):
        self.street = street
        self.city = city
        self.country = country
        self.contacts = contacts


class Contact:
    def __init__(self, type, value):
        self.type = type
        self.value = value

retrieved_data = collection.find_one()

retrieved_person = Person(
    retrieved_data['name'],
    retrieved_data['age'],
    Address(
        retrieved_data['address']['street'],
        retrieved_data['address']['city'],
        retrieved_data['address']['country'],
        [Contact(c['type'], c['value']) for c in retrieved_data['address']['contacts']]
    )
)

