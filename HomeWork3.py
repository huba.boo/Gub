# 1. Напишіть цикл, який буде вимагати від користувача ввести слово, в якому є буква "о"
# (враховуються як великі так і маленькі). Цикл не повинен завершитися, якщо користувач ввів слово без букви "о".


while True:
    user_input = input('Please enter any word: ').upper()
    if 'O' in user_input:
        print('Good job!')
        break


# 2. Є list з даними lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum'].
# Напишіть код, який свормує новий list (наприклад lst2), який містить лише змінні типу стрінг, які
# присутні в lst1. Зауважте, що lst1 не є статичним і може формуватися динамічно від запуску до запуску.

list1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
list2 = []
for item in list1:
    if type(item) == str:
        list2.append(item)
print(list2)

# 3. Є стрінг з певним текстом (можна скористатися input або константою). Напишіть код,
# який визначить кількість слів в цьому тексті, які закінчуються на "о" (враховуються як великі так і маленькі).

my_text = 'Hello! Guess what? I have no idea how to do "this", do "that" & do anything.'
my_text = my_text.upper()
words = my_text.split()
counter = 0
for word in words:
    if word.strip('.,!"&?').endswith('O'):
        counter +=1
print(counter)


