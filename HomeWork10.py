# Написати функцію, котра отримує стрічку та повертає цю стрічку, проте очищену від малих літер
# ("длоОЛО 55 ! " має повернути "ОЛО"). окрім того довжина стрічки не має перевищувати 25 символів
# (все що більше просто обрізається)
# покрити тестами
# перевірити flake8, mypy
# не забувайте про аннотації типів і докстрінги при потребі

$ flake8 clean_string.py
$ mypy clean_string.py


def clean_string(s: str) -> str:
    """
    Cleans a string by removing lowercase letters and truncating it to 25 characters or less.

    Args:
        s: The string to clean.

    Returns:
        The cleaned string.

    """
    cleaned = ''.join(c for c in s if not c.islower())
    return cleaned[:25]

assert clean_string("длоОЛО 55 ! ") == "ОЛО"
assert clean_string("aBcdeFGhIjKlmnoPqrStUvWxYZ") == "BFGIKPUSWYZ"
assert clean_string("abcdefghijklmnopqrstuvwxyz") == ""
assert clean_string("1234567890123456789012345") == "12345678901234567890123"
assert clean_string("") == ""
