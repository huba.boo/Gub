import threading


def function_without_parameters():
    print("Executing function_without_parameters")


def function_with_parameters(param):
    print(f"Executing function_with_parameters with param: {param}")


def run_tests():
    thread1 = threading.Thread(target=function_without_parameters)
    thread2 = threading.Thread(target=function_with_parameters, args=("example_param",))


    thread1.start()
    thread2.start()


    thread1.join()
    thread2.join()


if __name__ == "__main__":
    run_tests()



pip install pytest-xdist

import threading
import pytest


@pytest.mark.parametrize("param", [1, 2, 3])
def test_function_with_parameters(param):
    print(f"Running test_function_with_parameters with param: {param}")
    assert param > 0


def test_function_without_parameters():
    print("Running test_function_without_parameters")
    assert True


if __name__ == "__main__":
    pytest.main(["-n", "2"])

pytest -n 2
