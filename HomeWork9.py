# 1. Напишіть декоратор, який визначає час виконання функції. Заміряйте час іиконання функцій з попереднього ДЗ
# 2. Візьміть функції з попереднього ДЗ, покладіть їх у файл lib.py і імпортуйте в основний файл для виконання


import time

def timer(func):
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        print(f"Час виконання функції {func.__name__}: {end_time - start_time:.4f} секунд")
        return result
    return wrapper

@timer
def get_season(date_str):
    day, month = map(int, date_str.split("."))
    if month == 12 or month <= 2:
        return "зима"
    elif month <= 5:
        return "весна"
    elif month <= 8:
        return "літо"
    else:
        return "осінь"

import lib

date_str = "12.01"
season = lib.get_season(date_str)
print(f"Сезон: {season}")