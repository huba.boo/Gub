# Install PostgreSQL and PGAdmin4
# Create a database for 2 tables. One of them has a foreign key to another
# Fill a few rows with test data in both of them
# Create connection and cursor using psycopg2 in Python
# Get data from one of these tables using cursor and psycopg2
# Create a session with SQLAlchemy
# Create dbModels classes for your tables, using Base = declarative_base() and inheritance
# Create repositories for these tables with the next functional - get_all(), get_item_by_id(), remove_item(), update_item(), add_item()

pip install psycopg2 SQLAlchemy

import psycopg2
from sqlalchemy import create_engine, Column, Integer, String, ForeignKey
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.ext.declarative import declarative_base

conn = psycopg2.connect(
    host="your_host",
    port="your_port",
    database="your_database",
    user="your_username",
    password="your_password"
)
cursor = conn.cursor()

cursor.execute("""
    CREATE TABLE table1 (
        id SERIAL PRIMARY KEY,
        name VARCHAR(255)
    );
""")
cursor.execute("""
    CREATE TABLE table2 (
        id SERIAL PRIMARY KEY,
        table1_id INTEGER REFERENCES table1(id),
        data VARCHAR(255)
    );
""")
conn.commit()

cursor.execute("INSERT INTO table1 (name) VALUES ('Item 1'), ('Item 2')")
cursor.execute("INSERT INTO table2 (table1_id, data) VALUES (1, 'Data 1'), (2, 'Data 2')")
conn.commit()

Base = declarative_base()

class Table1(Base):
    __tablename__ = 'table1'
    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    table2 = relationship('Table2', backref='table1')

class Table2(Base):
    __tablename__ = 'table2'
    id = Column(Integer, primary_key=True)
    table1_id = Column(Integer, ForeignKey('table1.id'))
    data = Column(String(255))

class Table1Repository:
    def get_all(self):
        return session.query(Table1).all()

    def get_item_by_id(self, item_id):
        return session.query(Table1).get(item_id)

    def remove_item(self, item):
        session.delete(item)
        session.commit()

    def update_item(self, item):
        session.commit()

    def add_item(self, item):
        session.add(item)
        session.commit()

class Table2Repository:
    def get_all(self):
        return session.query(Table2).all()

    def get_item_by_id(self, item_id):
        return session.query(Table2).get(item_id)

    def remove_item(self, item):
        session.delete(item)
        session.commit()

    def update_item(self, item):
        session.commit()

    def add_item(self, item):
        session.add(item)
        session.commit()

