# Write a test to log in on the website https://admin-demo.nopcommerce.com/
# Credentials:
# Login - admin@yourstore.com
# Password - admin
# Enter the login and password fields and click the login button. Check that login was successful

import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


class LoginTest(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()  # Change the webdriver to the one installed on your system
        self.driver.get("https://admin-demo.nopcommerce.com/")

    def tearDown(self):
        self.driver.quit()

    def test_login(self):
        # Find the email and password input fields
        email_input = self.driver.find_element_by_id("Email")
        password_input = self.driver.find_element_by_id("Password")

        # Enter the login and password credentials
        email_input.send_keys("admin@yourstore.com")
        password_input.send_keys("admin")

        # Click the login button
        login_button = self.driver.find_element_by_xpath("//input[@value='Log in']")
        login_button.click()

        # Check that login was successful by verifying that the dashboard page is loaded
        dashboard_page = self.driver.find_element_by_xpath("//h1[text()='Dashboard']")
        self.assertTrue(dashboard_page.is_displayed())


if __name__ == '__main__':
    unittest.main()
