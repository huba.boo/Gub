from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BasePage:
    def __init__(self, driver):
        self.driver = driver


class Button(BasePage):
    def __init__(self, driver, locator):
        super().__init__(driver)
        self.locator = locator

    def click(self):
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(self.locator)).click()


class Label(BasePage):
    def __init__(self, driver, locator):
        super().__init__(driver)
        self.locator = locator

    def get_text(self):
        return WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(self.locator)).text


class Textbox(BasePage):
    def __init__(self, driver, locator):
        super().__init__(driver)
        self.locator = locator

    def set_text(self, text):
        element = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(self.locator))
        element.clear()
        element.send_keys(text)


class Driver:
    _instance = None

    @staticmethod
    def get_driver():
        if Driver._instance is None:
            Driver._instance = webdriver.Chrome()
        return Driver._instance


class TestPage:
    @classmethod
    def setup_class(cls):
        cls.driver = Driver.get_driver()

    @classmethod
    def teardown_class(cls):
        cls.driver.quit()

    def setup_method(self):
        self.driver.get("https://admin-demo.nopcommerce.com/")

    def test_login(self):
        email_textbox = Textbox(self.driver, (By.ID, "Email"))
        email_textbox.set_text("admin@yourstore.com")

        password_textbox = Textbox(self.driver, (By.ID, "Password"))
        password_textbox.set_text("admin")

        login_button = Button(self.driver, (By.CSS_SELECTOR, "[type='submit']"))
        login_button.click()

        dashboard_label = Label(self.driver, (By.CSS_SELECTOR, ".dashboard h1"))
        assert dashboard_label.get_text() == "Dashboard"
