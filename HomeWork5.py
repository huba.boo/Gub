# є url https://dummyjson.com/users (100 сторінок)
# вивести в консоль середній вік чоловіків з Brown волоссям, а також сформувати список людей, що
# проживають в місті Louisville
# обовязково прикріпити requirements.txt

import requests

url = "https://dummyjson.com/users?limit=100"
response = requests.get(url)

response = requests.get(url)
my_users = response.json()

users = my_users.get('users', [])

ages = []
louisville_users = []
for user in users:
    if user['gender'] == 'male' and user['hair']['color'].lower() == 'brown':
        ages.append(user['age'])

    if user['address'].get('city') == 'Louisville':
        louisville_users.append('name')
        print()
    else:
        print('No such users')

if ages:
    avg_age = sum(ages) / len(ages)
    print("Average Brown hair man age is: ", avg_age)
else:
    print('No such users')

