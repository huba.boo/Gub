import docker

client = docker.from_env()

data_dir = "<path-to-data-directory>"
logs_dir = "<path-to-logs-directory>"
server_port = "<port-on-host>"
agent_config_folder = "<path-to-agent-config-folder>"
server_url = "<url-to-TeamCity-server>"
container_name = "teamcity-server-instance"

server_container = client.containers.run(
    "jetbrains/teamcity-server",
    name=container_name,
    volumes={
        data_dir: {
            "bind": "/data/teamcity_server/datadir",
            "mode": "rw"
        },
        logs_dir: {
            "bind": "/opt/teamcity/logs",
            "mode": "rw"
        }
    },
    ports={
        "8111/tcp": (server_port, )
    },
    detach=True
)

agent_container = client.containers.run(
    "jetbrains/teamcity-agent",
    environment={
        "SERVER_URL": server_url
    },
    volumes={
        agent_config_folder: {
            "bind": "/data/teamcity_agent/conf",
            "mode": "rw"
        }
    },
    detach=True
)

command = "docker exec -u root -t -i {} /bin/bash".format(agent_container.id)

import os
os.system(command)
